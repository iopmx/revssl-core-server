# RevSSL Core server v1

Rev SSL is an application that allows you to create a secure SSL / TLS tunnel between the client and the server which makes use of the revSSL android application found in the playstore, now you can install revSSL on your private server and use it.


## Requeriments

- Linux X64 or any other system uses systemd -> recommended UBUNTU 20 
- Python3
- OpenSSL





## Installation




Install requeriments.
```sh
sudo apt update
```
```sh
sudo apt install python3 openssl git 
```


Clone this repository
```sh
cd <repo_dir>
```


Create self signed certificate and key 

```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 365 -nodes
```


Execute script install.py
```
python3 install.py
```

Press **y** for accept TOS and **ENTER**               

wait for finishing process install 

**Default port is 443 and 3128 and password is revSSL for change this edit file /etc/revss/rev.conf and restart service.**