
#before executing this script you need gen cert and key
# type this command openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 365 -nodes
TOS="""TERMS OF SERVICE

That he can not do:
1.You cannot use RevSSL on the client side in applications other than the official ones from newtoolsworks.com

2.Reverse engineering of this binary is prohibited

If you break any of these terms, the use of it on your servers may be prohibited.

What you can do:
1. Use the binary as SAAS.

2. Install the binary on any number of servers you have.

3. Share the binary with any individual as long as they use the binary downloaded from the repository or official links provided.

Every action you take is the responsibility of the end user who installs this binary on his server or servers.
"""
import os
import shutil
from time import sleep

print(TOS)
val = input("y/n: ")
if not val == "y":
    os._exit(1)


HOME_REVSSL= "/etc/revssl"

print("Home revSSL "+HOME_REVSSL)
if not os.getuid()  == 0:
    print("Run as root")
    os._exit(1)

if not os.path.exists("cert.pem") and not os.path.exists("cert.pem"):
    print("Please generate key and cert before installing")
    os._exit(1)

print("cert and key copied to home revssl ")

if not os.path.exists(HOME_REVSSL):
    os.makedirs(HOME_REVSSL)

print("Copying data")
shutil.copy("revSSL",HOME_REVSSL+"/revSSL")
shutil.copy("cert.pem",HOME_REVSSL+"/cert.pem")
shutil.copy("key.pem",HOME_REVSSL+"/key.pem")
shutil.copy("rev.conf",HOME_REVSSL+"/rev.conf")
shutil.copy("revssl.service","/etc/systemd/system/revssl.service")
os.system("systemctl daemon-reload")
os.system("service revssl start")
print("Service started")
sleep(2)
print("*****************************************************")
print("If you want modify please edit /etc/revssl/srev.conf")
print("and restart service service revssl restart")
print("*****************************************************")

